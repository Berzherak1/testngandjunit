package Lesson1.Homework1.Task3;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;

@Test
public class CreateAndDeleteTempDirectory {
    public static Path path;

    @BeforeSuite
    public Path createDirectory() throws IOException {
        path = Files.createTempDirectory("TEMP");
        System.out.println(path);
        return path;
    }

    @AfterSuite
    public void deleteDirectory() throws IOException {
        Files.delete(path);
    }
}


