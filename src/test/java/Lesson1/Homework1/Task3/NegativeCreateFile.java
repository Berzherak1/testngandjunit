package Lesson1.Homework1.Task3;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOError;
import java.io.IOException;

@Test
public class NegativeCreateFile {
    public File file = null;
    public File file2 = null;

    public void createFileWithWrongPath() {
        file = new File("Zzz:" + "/" + "art2.txt");
        boolean bool = false;
        try {
            bool = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(file + " файл не создан, т.к. " + e.getMessage());
        }
    }

    public void createFileWithNullObject() {
        boolean bool = false;
        try {
            bool = file2.createNewFile();
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
            System.out.println(file2 + " файл не создан, т.к. " + e.getMessage());
        }
    }
}

