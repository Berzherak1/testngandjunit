package Lesson1.Homework1.Task3;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

@Test
public class PositiveCreateFileAndDelete {
    public File file;

    public void createFile() throws IOException {
        System.out.println("Create");
        file = new File(CreateAndDeleteTempDirectory.path + "/" + "art2.txt");
        boolean bool = file.createNewFile();
    }

    public void deleteFile() {
        System.out.println("delete");
        file.delete();
        file = null;
    }


}








