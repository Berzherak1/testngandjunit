package Lesson1.sample2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

@Test
public class FirstClass2 {
 WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.out.println("LaunchChrome");
        driver = new ChromeDriver();
    }

    @AfterClass
    public void setDown() {
        System.out.println("SetDown");
        driver.quit();
    }

}
